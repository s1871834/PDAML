
# coding: utf-8

# # Using Pandas to explore the *Countries of the world* dataset

# This notebook provides examples on how to use **Pandas** for the analysis of a sample dataset. Here we will be using the **countries of the world** dataset for illustration. 
# 
# The source data has been made available in the course Git repository and is also available via the [Kaggle Datasets page](https://www.kaggle.com/fernandol/countries-of-the-world/home).
# 
# Please note that this notebook is not intended to be a complete Pandas tutorial! Please visit the [Pandas webpage](http://pandas.pydata.org/) for a reference on how to use the methods described below and more in-depth examples.

# **Note for running standalone Python version**
# - In order to see the output of any of the steps enclose the statement with a `print` statement (e.g `c.head(3)` to `print(c.head(3)`).
# - Plots should be rendered automatically. To supress plots comment the  `plt.show` or `ax.plot` where appropiate
# 
# **Running in Python 2**
# - The notebook was written in Python 3 but should (hopefully) just work in Python 2. Uncomment the line below if you are using the `print` syntax from Python 3 

# In[ ]:


#from __future__ import print_function # print function from Python3


# ### Import SciPy Modules

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
#get_ipython().magic(u'matplotlib inline # comment this in standalone version')


# ### Data Cleaning

# The source dataset is provided in CSV format so no data preparation is needed before we import the file into a Pandas DataFrame. The `read_csv` method takes the data from file and constructs a new `DataFrame`:

# In[2]:


# read CSV file
c = pd.read_csv('countries.csv')


# Before we start any analysis lets first inspect the data to see if it has been read correctly and makes sense. To view a small snippet of the data frame use the `head` method:

# In[3]:


c.head(3)


# To see how many rows and columns the dataframe holds use the `shape` method:

# In[4]:


c.shape


# So there are in total 227 countries listed and 20 columns (or features).
# 
# The data seems to have been read correctly at first glance but on closer inpection there are commas instead of decimal places in some of the columns. This can be fixed with the `decimal` parameter in the `read_csv` method:

# In[5]:


# read CSV file again
c = pd.read_csv('countries.csv', decimal=",")
c.head(3)


# Lets now look at the column names:

# In[6]:


c.columns


# As part of the data exploration we will continually reference the column names during filtering and querying. The imported column names have extraneous characters (e.g. `%`) which will be inconvenient to reference in the code so it is worthwhile at this stage to simplify the columns before we perform any analysis steps: 

# In[7]:


c.columns = ['Country', 'Region', 'Population', 'Area', 'Density', 'Coastline', 'Migration', 'Infant mortality',              'GDP', 'Literacy', 'Phones', 'Arable', 'Crops', 'OtherLand', 'Climate', 'Birthrate', 'Deathrate',              'Agriculture', 'Industry', 'Service']


# Some care will also needed with using string data when applying pattern matches. First consider values in the `Region` column. To select a single column from the DataFrame use the following notation:  

# In[48]:


c.Region.head()


# In[9]:


type(c.Region)


# This provides all the entries of the column in an object of type `Series`
# 
# Here we are only interested in the unique entries in the column. This can be provided by the `unique` method:

# In[10]:


c.Region.unique()


# You can see lots of spaces at the end of each entry. These can be removed using the `strip` method in `str`:

# In[11]:


c.Region = c.Region.str.strip()
c.Region.unique()


# Here we have stripped the whitespace in each entry and assigned this back to the original DataFrame. 
# 
# Similarly in the `Country` colmnn there is an extra space at the end of each value that needs to be stripped:

# In[12]:


c.Country.head(5).tolist()


# In[13]:


c.Country = c.Country.str.strip()
c.Country.head(5).tolist()


# Note that we have used `tolist` to change the format from a `Series` to a python `list` object.
# 
# Alternatively, a strip of **all** string columns in the DataFrame can be done using a `lambda` expression in the `applymap` method:

# In[14]:


c = c.applymap(lambda x: x.strip() if type(x) is str else x)


# Before we proceed to data exploration let us check whether all the entries have meaningful data in each of the columns: 

# In[15]:


c.count()
#c.info() # alternative


# From `count` and `info` we can see that not all of the 227 counties have entries in each column. For example, 22 countries have missing climate data.
# 
# It is important to perform this step when conducting an analysis over features with missing data. The `dropna` method is used to remove any null entries:

# In[16]:


lit = c.Literacy.dropna()
lit.shape # now has 209 entries (none of type Null)


# We can see from `shape` that the `Series` now has only 209 entries instead of 227. 
# 
# Let us look at the structure of the `DataFrame` again:

# In[17]:


c.head(3)


# The index is on the left (0,1,2) is meaningless for any analysis and it would be best to change the index to `Country`. This can be done with `set_index`:  

# In[18]:


c.set_index('Country', inplace=True)
c.head(3)


# Our source data has now been sanitised!

# ### Extraction and Slicing

# To extract selected rows from a large dataset use `iloc` to slice by indices: 

# In[19]:


c.iloc[2:5] 
#c[2:5] # alternative 


# To find a specific country in the index use `loc`: 

# In[20]:


c.loc['Belgium']


# A list can also be provided to `loc`: 

# In[21]:


countries = ['Belgium', 'France', 'Spain']
c.loc[countries]


# Slicing can be combined with column selection:  

# In[22]:


c[2:5].Population


# This generates a `Series` object. To generate a NumPy `array` or python `list` use the following methods:

# In[23]:


c[2:5].Population.values # numpy array
c[2:5].Population.tolist() # python list 


# To display multiple columns:

# In[24]:


features = ['Population', 'GDP']
c[features].head(3)


# ### Querying

# Entries can be filtered by applying query statements.
# 
# For example, lets select countries in the Baltic region:

# In[25]:


c[c.Region == 'BALTICS'] 
# c.query('Region == "BALTICS"') # alternative using query() 
# c.Region.str.match('BALTICS') # alternative using str.match()


# This can be combined with selecting columns using `loc`

# In[26]:


features = ['Population', 'GDP']
c.loc[c.Region == 'BALTICS', features]


# The filter itself is a `Series` of boolean values that can be applied as a mask to the `DataFrame`: 

# In[27]:


qry = c.Region == 'BALTICS'
type(qry)
# qry # print to see True/False identities based on condition test


# `Series` methods can also applied as filters. For example, instead of looking for an exact string match we may wish to query whether a `Region` is one of a selected list: 

# In[28]:


easteurope = ['BALTICS', 'EASTERN EUROPE'] 
features = ['Region', 'Population', 'GDP']
c.loc[c.Region.isin(easteurope), features]


# More complex queries can be constructed using boolean operators. For example lets extract data from countries in Eastern Europe and the Baltics have a population of less than 3 million: 

# In[29]:


qry = (c.Region.isin(easteurope)) & (c.Population < 3000000) 
c[qry]


# ### Generating Summary Data

# We would now like to start providing summary statistics for extracted data. Lets start by counting the number of countries in each region:

# In[30]:


# display value counts of Region
c.Region.value_counts()


# `sort_values()` can then order the results in ascending or descending order:

# In[31]:


c.Region.value_counts().sort_values() # sort by ascending 
# c.Region.value_counts().sort_values(ascending=False) # sort by descending


# `sort_values` can also be applied to multiple columns with the sort field chosen using the `by` parameter.
# 
# For example, if we wanted to display data from countries which had the highest GDP:

# In[32]:


c.sort_values(by='GDP', ascending=False).head(3)


# Summary statistics can be generated from groups of data using the `groupby` method. For example to extract the mean birth rate per geographical region: 

# In[33]:


c.groupby('Region').Birthrate.mean().sort_values(ascending=False)


# The long chain of method calls above can be split into individual calls if this improves clarity:

# In[34]:


region = c.groupby('Region')
birthrate = region.Birthrate
meanbirth = birthrate.mean()
meanbirth.sort_values(ascending=False)


# `describe` and `agg` are helper methods for `Series` and `DataFrame` objects to provide useful summary statistics:

# In[35]:


c.groupby('Region').Birthrate.describe()
# c.groupby('Region').Birthrate.agg(['count', 'max','mean']) # select subset of summary data


# ### Appending Data

# We may also wish to add new features to our data based on entries in other columns. 
# 
# In this example we classify population values into five categories:

# In[36]:


ptype = []
for p in c.Population: 
    p /= 1000000
    if (p < 1):
        ptype.append('tiny')
    elif ((p >= 1) and (p < 10)):
        ptype.append('small')
    elif ((p >= 10) and (p < 100)):
        ptype.append('medium') 
    elif ((p >= 100) and (p < 1000)):
        ptype.append('large')
    elif (p >= 1000):
        ptype.append('huge')


# In[37]:


c['PopulationType'] = pd.Series(ptype, index=c.index)


# In[38]:


features = ['Population', 'PopulationType']
c[features].sort_values(by='Population', ascending=False).head(5)


# ### Iterating over Data

# In an analysis we might wish to apply operations per row using multiple features. `iterrows` will provide each row in the DataFrame as an iterator than can be used in a `for` loop. 
# 
# A simple example below generates the Population/GDP ratio for each country:

# In[39]:


ratio = {}
for index, row in c.iterrows():
    ratio[index] = row['Population'] / row['GDP']
ratio['Spain']    


# ### Visualisation 

# Now that we can query and extract any portion of our dataset we can create visualisations using `matplotlib`.
# 
# `Series` object types can recognised by `matplotlib` and do not have to be converted beforehand. We can therefore plot a histogram of the distribution of values from the columns in just a few steps.
# 
# As an example lets look at how land is used in each country: 

# In[47]:


#plt.hist(c.Arable, bins=50, range=(0,100)) # this will not work!
plt.hist(c.Arable.dropna(), bins=50, range=(0,100)) # null value clean
plt.title("Arable land")
plt.xlabel("Proportion (%)")
plt.ylabel("Count")
plt.show()


# Here you see the `dropna()` method was applied to the Arable series to avoid problems with `Null` entries. 
# 
# Side by side comprarison plots can also be generated in the same way:

# In[41]:


# TODO - better orientation
plt.figure(figsize=(12.0, 3.0))

plt.subplot(131)
plt.hist(c.Arable.dropna(), bins=50, range=(0,100))
plt.title("Arable")
plt.xlabel("Proportion (%)")
plt.ylabel("Count")

plt.subplot(132)
plt.hist(c.Crops.dropna(), bins=50, range=(0,100))
plt.title("Crops")
plt.xlabel("Proportion (%)")
plt.ylabel("Count")

plt.subplot(133)
plt.hist(c.OtherLand.dropna(), bins=50, range=(0,100))
plt.title("Other Land")
plt.xlabel("Proportion (%)")
plt.ylabel("Count")

plt.show()


# We can also generate the same plots by applying `plot` and `hist` methods directly to a `Series`:

# In[56]:


# alternative 
ax = c.Arable.dropna().hist(bins=50, range=(0,100), grid=False)
ax.set_title('Arable')
ax.set_xlabel('Proportion (%)')
ax.set_ylabel('Count')
plt.show()


# For generating multiple plots:

# In[55]:


c.loc[c.Region == 'EASTERN EUROPE', ['Birthrate', 'Literacy']].dropna().hist(bins=10, grid=False, color='r', alpha=0.5)
plt.show()


# `plot` can be used to access other types of plots - such as a horizontal bar plot.
# 
# Here we show the top 10 countries with the highest phone ownership:

# In[57]:


c.Phones.sort_values(ascending=False).head(10).plot(kind='barh')
plt.show()


# `plot` can be applied to a `DataFrame` object to generate scatter plots.
# 
# For example, lets use a scatter plot to investigate the relationship between phone ownership and GDP:

# In[58]:


c.plot.scatter(x='Phones', y='GDP', color='black')
plt.show()


# By visual inspection there appears to be a correlation between these two features - so lets perform a simple linear regression to determine the strength of the relationship:

# In[59]:


# create new dataframe with features of interest
d = c[['Phones', 'GDP']].dropna()

# perform linear regression
gradient,intercept,r_value,p_value,std_err=stats.linregress(d.Phones, d.GDP)

print("regression results:")
print(" gradient %f\n intercept: %f\n r_value: %f\n p_value %f\n std_err %f\n" %       (gradient,intercept,r_value,p_value,std_err))

# plot scatter
c.plot.scatter(x='Phones', y='GDP', color='black')

# plot best fit line
X = list(range(0,int(d.Phones.max()),100)) # number of points is arbitrary
yfit = [intercept + gradient * xi for xi in X]
plt.plot(X, yfit, color='red', linestyle='dashed', linewidth=3)
plt.show()


# The r-value suggests that phone ownership is a good predictor of GDP - but does correlation mean causation? I will leave that up to you to decide! 
